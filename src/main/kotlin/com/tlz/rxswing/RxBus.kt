package com.tlz.rxswing

import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit

private val subjectMapper = ConcurrentHashMap<Any, ArrayList<PublishSubject<Any>>>()

fun <T> onEvent(observable: Observable<T>, onNext: (T) -> Unit,
                onError: (Throwable) -> Unit): Disposable = observable.subscribe(onNext, onError)

fun <T> onEvent(observable: Observable<T>, onNext: (T) -> Unit): Disposable =
    onEvent(observable, onNext, { it.printStackTrace() })

fun <T> onEvent(tag: Any, onNext: (T) -> Unit): Disposable = onEvent(register(tag), onNext)

fun <T> onEvent(tag: Any, onNext: (T) -> Unit,
                onError: (Throwable) -> Unit): Disposable = onEvent(register(tag), onNext, onError)

fun <T> register(tag: Any): Observable<T> {
  var subjectList = subjectMapper[tag]
  if (null == subjectList) {
    subjectList = ArrayList()
    subjectMapper.put(tag, subjectList)
  }
  val subject = PublishSubject.create<T>()
  subjectList.add(subject as PublishSubject<Any>)
  return subject
}

fun unregister(tag: Any) {
  val subjects = subjectMapper[tag]
  if (null != subjects) {
    subjectMapper.remove(tag)
  }
}

fun unregister(tag: Any, observable: Observable<*>) {
  val subjectList = subjectMapper[tag]
  if (null != subjectList) {
    subjectList.remove(observable)
    if (subjectList.isEmpty()) {
      subjectMapper.remove(tag)
    }
  }
}

fun post(content: Any) {
  post(content.javaClass, content)
}

fun post(tag: Any, content: Any) {
  postDelay(tag, content, 0L)
}

fun postDelay(content: Any, millis: Long): Disposable? = postDelay(content, millis, TimeUnit.MILLISECONDS)

fun postDelay(content: Any, delay: Long, unit: TimeUnit): Disposable? =
    postDelay(content.javaClass, content, delay, unit)

fun postDelay(tag: Any, content: Any, millis: Long): Disposable? =
    postDelay(tag, content, millis, TimeUnit.MILLISECONDS)

fun postDelay(tag: Any, content: Any, delay: Long, unit: TimeUnit): Disposable? {
  if (delay == 0L) {
    val subjectsList = subjectMapper[tag]
    if (subjectsList?.isNotEmpty() == true) {
      subjectsList.filter { it.hasObservers() }.forEach { it.onNext(content) }
    }
  } else {
    return delay(delay, unit) {
      val subjectsList = subjectMapper[tag]
      if (subjectsList?.isNotEmpty() == true) {
        subjectsList.filter { it.hasObservers() }.forEach { it.onNext(content) }
      }
    }
  }
  return null
}