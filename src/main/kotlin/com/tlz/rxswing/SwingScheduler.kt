package com.tlz.rxswing

import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables
import java.awt.EventQueue
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.util.concurrent.TimeUnit
import javax.swing.SwingUtilities
import javax.swing.Timer


class SwingScheduler: Scheduler() {

  override fun createWorker(): Scheduler.Worker {
    return InnerSwingScheduler()
  }

  private class InnerSwingScheduler : Scheduler.Worker() {

    private var disposed: Boolean = false

    override fun isDisposed(): Boolean = disposed

    override fun schedule(run: Runnable, delay: Long, unit: TimeUnit): Disposable {
      if (disposed) {
        return Disposables.disposed()
      }
      val resultDelay = Math.max(0, unit.toMillis(delay))
      assertThatTheDelayIsValidForTheSwingTimer(delay)

      class ExecuteOnceAction: ActionListener{
        private var timer: Timer? = null

        fun setTimer(timer: Timer) {
          this.timer = timer
        }
        override fun actionPerformed(e: ActionEvent?) {
          timer?.stop()
          if (disposed) {
            return
          }
          if (SwingUtilities.isEventDispatchThread()){
            run.run()
          } else {
            EventQueue.invokeLater(run)
          }
        }
      }
      val executeOnce = ExecuteOnceAction()
      val timer = Timer(resultDelay.toInt(), executeOnce)
      executeOnce.setTimer(timer)
      timer.start()
      return object: Disposable{
        private var disposed = false
        override fun isDisposed(): Boolean = disposed

        override fun dispose() {
          disposed = true
          this@InnerSwingScheduler.disposed = true
          timer.stop()
        }
      }
    }

    override fun dispose() {
      disposed = true
    }
  }


  companion object {
    private val INSTANCE = SwingScheduler()

    private fun assertThatTheDelayIsValidForTheSwingTimer(delay: Long) {
      if (delay < 0 || delay > Integer.MAX_VALUE) {
        throw IllegalArgumentException(String.format("The swing timer only accepts non-negative delays up to %d milliseconds.", Integer.MAX_VALUE))
      }
    }

    fun getInstance(): SwingScheduler {
      return INSTANCE
    }
  }
}