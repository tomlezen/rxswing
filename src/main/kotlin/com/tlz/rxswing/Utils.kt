package com.tlz.rxswing

import io.reactivex.Flowable
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.CoroutineScope
import kotlinx.coroutines.experimental.CoroutineStart
import java.util.concurrent.TimeUnit
import javax.swing.SwingUtilities

fun delay(millisDelayTime: Long, block: () -> Unit): Disposable = delay(millisDelayTime, TimeUnit.MILLISECONDS, block)

fun delay(delayTime: Long, timeUnit: TimeUnit, block: () -> Unit): Disposable {
  return Flowable.timer(delayTime, timeUnit)
      .subscribe { block() }
}

fun <T> async(start: CoroutineStart = CoroutineStart.DEFAULT, block: suspend CoroutineScope.() -> T) =
    kotlinx.coroutines.experimental.async(CommonPool, start, block)


fun ui(block: () -> Unit) {
  return SwingUtilities.invokeLater {
    block()
  }
}

fun delayForMainThread(delayTime: Long, block: () -> Unit): Disposable =
    delayForMainThread(delayTime, TimeUnit.MILLISECONDS, block)

fun delayForMainThread(delayTime: Long, timeUnit: TimeUnit, block: () -> Unit): Disposable {
  return Flowable.timer(delayTime, timeUnit)
      .observeOn(SwingScheduler.getInstance())
      .subscribe { block() }
}